package org.test.gamesys.entities;

import java.math.BigDecimal;

/**
 * Created by lich on 10/31/15.
 */
public class Win {

    public static final String WIN = "Win";
    public static final String LOSE = "Lose";

    private String bet;
    private BigDecimal betAmount;
    private BigDecimal wonAmount;
    private String outcome;

    public BigDecimal getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(BigDecimal betAmount) {
        this.betAmount = betAmount;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public String getBet() {
        return bet;
    }

    public void setBet(String bet) {
        this.bet = bet;
    }

    public BigDecimal getWonAmount() {
        return wonAmount;
    }

    public void setWonAmount(BigDecimal wonAmount) {
        this.wonAmount = wonAmount;
    }
}
