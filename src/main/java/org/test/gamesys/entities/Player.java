package org.test.gamesys.entities;

import java.math.BigDecimal;

/**
 * Created by lich on 10/31/15.
 */
public class Player {

    private boolean hasTotalValues = false;

    private String name;
    private BigDecimal totalWin;
    private BigDecimal totalBet;

    public boolean hasTotalValues() {
        return hasTotalValues;
    }

    public void setHasTotalValues(boolean hasTotalValues) {
        this.hasTotalValues = hasTotalValues;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getTotalWin() {
        return totalWin;
    }

    public void setTotalWin(BigDecimal totalWin) {
        this.totalWin = totalWin;
    }

    public BigDecimal getTotalBet() {
        return totalBet;
    }

    public void setTotalBet(BigDecimal totalBet) {
        this.totalBet = totalBet;
    }
}
