package org.test.gamesys.entities;

import java.math.BigDecimal;

/**
 * Created by lich on 10/31/15.
 */
public class Bet {

    public static final String ODD = "ODD";
    public static final String EVEN = "EVEN";

    private String owner;
    private int number;
    private String oddEven;
    private BigDecimal amount;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getOddEven() {
        return oddEven;
    }

    public void setOddEven(String oddEven) {
        this.oddEven = oddEven;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
