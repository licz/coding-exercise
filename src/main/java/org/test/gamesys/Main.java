package org.test.gamesys;

import org.test.gamesys.game.RouletteGame;

/**
 * Created by lich on 10/31/15.
 */
public class Main {

    public static final String PLAYERS_FILE = "players_list.txt";

    public static void main(String...args) {
        RouletteGame rouletteGame = new RouletteGame();
        rouletteGame.play();
    }
}
