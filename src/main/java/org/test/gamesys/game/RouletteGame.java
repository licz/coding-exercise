package org.test.gamesys.game;

import org.test.gamesys.Main;
import org.test.gamesys.api.*;
import org.test.gamesys.api.implementations.*;
import org.test.gamesys.entities.Player;

import java.io.InputStream;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by lich on 10/31/15.
 * Main game engine that connects the modules and also spins the roulette every 30 seconds.
 */
public class RouletteGame implements Runnable {

    private Croupier croupier;
    private RouletteTable rouletteTable;
    private StreamProcessor streamProcessor;
    private UserInterface userInterface;
    private DataParser dataParser;

    private List<Player> players;

    ScheduledExecutorService executor =
            Executors.newSingleThreadScheduledExecutor();

    public void play() {
        prepareTheGame();

        userInterface.start();

        executor.scheduleAtFixedRate(this, 30, 30, TimeUnit.SECONDS);
    }

    private void prepareTheGame() {
        dataParser = new DataParserImplementation();
        streamProcessor = new StreamProcessorImplementation(dataParser);
        rouletteTable = new RouletteTableImplementation();
        croupier = new CroupierImplementation();

        InputStream inputStream = this.getClass().getClassLoader()
                .getResourceAsStream(Main.PLAYERS_FILE);
        players = streamProcessor.readStream(inputStream);

        userInterface = new UserInterfaceImplementation(players, dataParser, new Scanner(System.in));
    }

    @Override
    public void run() {

        croupier.collectBets(userInterface.getBets());

        int winningNumber = rouletteTable.getWinningNumber();

        userInterface.showResults(croupier.calculateWinners(winningNumber), winningNumber);

        userInterface.showTotalResults();

        userInterface.clearBets();
    }
}
