package org.test.gamesys.api;

import org.test.gamesys.entities.Bet;
import org.test.gamesys.entities.Player;
import org.test.gamesys.entities.Win;

import java.util.List;
import java.util.Map;

/**
 * Created by lich on 10/31/15.
 * Communicates with user
 */
public interface UserInterface {

    /*
     * starts the communication with user
     */
    public void start();

    /*
     * returns bets that were collected
     * @return      map of players and their bets
     */
    public Map<Player, List<Bet>> getBets();

    /*
     * clears bets to prepare for next game
     */
    public void clearBets();

    /*
     * shows results of the game
     */
    public void showResults(Map<Player, List<Win>> wins, int winningNumber);

    /*
     * shows results of total scoring
     */
    public void showTotalResults();
}
