package org.test.gamesys.api;

import org.test.gamesys.entities.Bet;
import org.test.gamesys.entities.Player;

import java.util.List;

/**
 * Created by lich on 10/31/15.
 * Parses data strings into objects
 */
public interface DataParser {
    /*
     * Parses input text and returns Player Object
     */
    public Player parsePlayer(String text);

    /*
     * Parses input text, validates it against players list and returns Bet Object
     * @param  input    string that was entered by user
     * @param  players  list of all registered users
     * @return      Bet object
     */
    public Bet parseBet(String input, List<Player> players);
}
