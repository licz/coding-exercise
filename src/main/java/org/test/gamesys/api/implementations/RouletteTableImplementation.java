package org.test.gamesys.api.implementations;

import org.test.gamesys.api.RouletteTable;

import java.util.Random;

/**
 * Created by lich on 10/31/15.
 */
public class RouletteTableImplementation implements RouletteTable {
    @Override
    public int getWinningNumber() {
        return new Random().nextInt(36);
    }
}
