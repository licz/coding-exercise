package org.test.gamesys.api.implementations;

import org.test.gamesys.api.DataParser;
import org.test.gamesys.api.StreamProcessor;
import org.test.gamesys.entities.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by lich on 10/31/15.
 */
public class StreamProcessorImplementation implements StreamProcessor{

    private DataParser dataParser;

    public StreamProcessorImplementation(DataParser dataParser) {
        this.dataParser = dataParser;
    }

    @Override
    public List<Player> readStream(InputStream inputStream) {

        if (inputStream == null) {
            throw new RuntimeException("Could not find file!");
        }

        List<Player> players = new ArrayList<>();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line;

        try {
            while ((line = bufferedReader.readLine()) != null) {
                players.add(dataParser.parsePlayer(line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (players.isEmpty()) {
            throw new RuntimeException("Input file is empty!");
        }

        return players;
    }
}
