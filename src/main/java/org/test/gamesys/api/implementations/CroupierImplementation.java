package org.test.gamesys.api.implementations;

import org.apache.commons.lang.StringUtils;
import org.test.gamesys.api.Croupier;
import org.test.gamesys.entities.Bet;
import org.test.gamesys.entities.Player;
import org.test.gamesys.entities.Win;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lich on 10/31/15.
 */
public class CroupierImplementation implements Croupier {

    Map<Player, List<Bet>> bets;

    @Override
    public void collectBets(Map<Player, List<Bet>> bets) {
        this.bets = bets;
    }

    @Override
    public Map<Player, List<Win>> calculateWinners(int winningNumber) {
        Map<Player, List<Win>> wins = new HashMap<>();
        Win win = null;
        boolean even;
        for (Map.Entry<Player, List<Bet>> entry : bets.entrySet()) {
            wins.put(entry.getKey(), new ArrayList<Win>());
            for (Bet bet : entry.getValue()) {
                if (StringUtils.isEmpty(bet.getOddEven())) {
                    if (winningNumber == bet.getNumber()) {
                        win = new Win();
                        win.setWonAmount(bet.getAmount().multiply(new BigDecimal(36)));
                        win.setBetAmount(bet.getAmount());
                        win.setBet(Integer.toString(bet.getNumber()));
                        win.setOutcome(Win.WIN);
                    } else {
                        win = generateLostBet(Integer.toString(bet.getNumber()), bet.getAmount());
                    }
                } else {
                    even = winningNumber%2 == 0;
                    if (winningNumber != 0 && (Bet.EVEN.equals(bet.getOddEven()) && even) || (Bet.ODD.equals(bet.getOddEven()) && !even)) {
                        win = new Win();
                        win.setWonAmount(bet.getAmount().multiply(new BigDecimal(2)));
                        win.setBetAmount(bet.getAmount());
                        win.setBet(bet.getOddEven());
                        win.setOutcome(Win.WIN);
                    } else {
                        win = generateLostBet(bet.getOddEven(), bet.getAmount());
                    }
                }
                wins.get(entry.getKey()).add(win);
            }
        }
        return wins;
    }

    private Win generateLostBet(String bet, BigDecimal betAmount) {
        Win win = new Win();
        win.setBetAmount(betAmount);
        win.setWonAmount(BigDecimal.ZERO);
        win.setBet(bet);
        win.setOutcome(Win.LOSE);
        return win;
    }
}
