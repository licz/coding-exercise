package org.test.gamesys.api.implementations;

import org.test.gamesys.api.DataParser;
import org.test.gamesys.api.UserInterface;
import org.test.gamesys.entities.Bet;
import org.test.gamesys.entities.Player;
import org.test.gamesys.entities.Win;

import java.util.*;

/**
 * Created by lich on 10/31/15.
 */
public class UserInterfaceImplementation implements Runnable, UserInterface {

    private Map<Player, List<Bet>> bets = new HashMap<>();
    private List<Player> players;
    private DataParser dataParser;
    private Scanner scanner;

    public UserInterfaceImplementation(List<Player> players, DataParser dataParser, Scanner scanner) {
        this.players = players;
        this.dataParser = dataParser;
        this.scanner = scanner;
    }

    @Override
    public void start() {
        sayHi();

        new Thread(this).start();
    }

    @Override
    public void run() {
        String line = null;

        while (true) {

            if ((line = scanner.nextLine()) != null) {
                Bet bet;
                try {
                    bet = dataParser.parseBet(line, players);
                    assignBet(bet);
                } catch (IllegalArgumentException | ArrayIndexOutOfBoundsException e) {
                    System.out.println("Incorrect format of bet!");
                }
            }
        }
    }

    private void assignBet(Bet bet) {
        if (bets.containsKey(getPlayer(bet.getOwner()))) {
            bets.get(getPlayer(bet.getOwner())).add(bet);
        } else {
            List<Bet> betList = new ArrayList<>();
            betList.add(bet);
            bets.put(getPlayer(bet.getOwner()), betList);
        }

        System.out.println("Okay! One more bet?");
    }

    private Player getPlayer(String name) {
        Player result = null;

        for (Player player : players) {
            if (player.getName().equals(name)) {
                result = player;
            }
        }

        return result;
    }

    @Override
    public Map<Player, List<Bet>> getBets() {
        return bets;
    }

    @Override
    public void clearBets() {
        bets = new HashMap<>();
    }

    @Override
    public void showResults(Map<Player, List<Win>> wins, int winningNumber) {
        newLine();
        System.out.println("Winning number: " + winningNumber);
        System.out.format("%15s %10s %10s %10s \n", "Player", "Bet", "Outcome", "Winnings");

        for (Map.Entry<Player, List<Win>> entry : wins.entrySet()) {
            for (Win win : entry.getValue()) {
                System.out.format("%15s %10s %10s %10s \n", entry.getKey().getName(), win.getBet(), win.getOutcome(), win.getWonAmount());
            }
        }

        newLine();

        updateTotalResults(wins);
    }

    protected void updateTotalResults(Map<Player, List<Win>> wins) {
        for (Map.Entry<Player, List<Win>> entry : wins.entrySet()) {
            for (Win win : entry.getValue()) {
                if (entry.getKey().hasTotalValues()) {
                    entry.getKey().setTotalWin(entry.getKey().getTotalWin().add(win.getWonAmount()));
                    entry.getKey().setTotalBet(entry.getKey().getTotalBet().add(win.getBetAmount()));
                }
            }
        }
    }

    @Override
    public void showTotalResults() {
        System.out.println("Total scores:");
        System.out.format("%15s %10s %10s", "Player", "Total Win", "Total Bet");
        newLine();
        for (Player player : players) {
            if (player.hasTotalValues()) {
                System.out.format("%15s %10s %10s \n", player.getName(), player.getTotalWin(), player.getTotalBet());
            }
        }
        newLine();
    }

    private void sayHi() {
        System.out.println("Welcome to the ConsoleRoulette!");
        System.out.println("Players at the table: ");
        for (Player player : players) {
            System.out.print(player.getName() + " ");
        }
        System.out.println("\nPlace a bet in format: Your_name number/oddEven amount");
    }

    private void newLine(){
        System.out.println();
    }

}
