package org.test.gamesys.api.implementations;

import org.apache.commons.lang.StringUtils;
import org.test.gamesys.api.DataParser;
import org.test.gamesys.entities.Bet;
import org.test.gamesys.entities.Player;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;

/**
 * Created by lich on 10/31/15.
 */
public class DataParserImplementation implements DataParser {

    @Override
    public Player parsePlayer(String text) {

        if (StringUtils.isEmpty(text)) {
            throw new RuntimeException("Line was empty!");
        }

        if (text.split(",").length == 2 || text.split(",").length > 3) {
            throw new IllegalArgumentException("Incorrect number of parameters!");
        }

        String name = text.split(",")[0];
        Player player = new Player();

        player.setName(name);

        if (text.split(",").length == 3) {
            player.setHasTotalValues(true);

            String totalWin = text.split(",")[1];
            String totalBet = text.split(",")[2];

            player.setTotalWin(new BigDecimal(totalWin));
            player.setTotalBet(new BigDecimal(totalBet));
        }

        return player;
    }

    @Override
    public Bet parseBet(String input, List<Player> players) {

        String name = input.split(" ")[0];
        String whatBet = input.split(" ")[1];
        String amount = input.split(" ")[2];

        Bet bet = new Bet();
        if (Bet.EVEN.equals(whatBet) || Bet.ODD.equals(whatBet)) {
            bet.setOddEven(whatBet);
        } else {
            int integerBet = Integer.parseInt(whatBet);
            if (integerBet<0 || integerBet > 36) {
                throw new IllegalArgumentException();
            }
            bet.setNumber(integerBet);
        }
        bet.setAmount(new BigDecimal(amount));
        if (bet.getAmount().compareTo(BigDecimal.ZERO)<0) {
            throw new IllegalArgumentException();
        }

        if (isCorrectName(players, name)) {
            bet.setOwner(name);
        } else {
            throw new IllegalArgumentException();
        }

        return bet;
    }

    protected boolean isCorrectName(List<Player> players, String name) {
        boolean result = false;
        for (Player player : players) {
            if (player.getName().equals(name)) {
                result = true;
                break;
            }
        }
        return result;
    }
}
