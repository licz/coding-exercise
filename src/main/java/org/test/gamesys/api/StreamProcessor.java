package org.test.gamesys.api;

import org.test.gamesys.entities.Player;

import java.io.InputStream;
import java.util.List;

/**
 * Created by lich on 10/31/15.
 * Reads file to start the game
 */
public interface StreamProcessor {

    /*
     * Creates player objects from stream
     * @param  inputStream  stream to read
     * @return      List of Players
     */
    public List<Player> readStream(InputStream inputStream);
}
