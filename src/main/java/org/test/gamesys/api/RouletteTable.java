package org.test.gamesys.api;

/**
 * Created by lich on 10/31/15.
 * Generates and returns winning numbers
 */
public interface RouletteTable {

    /*
     * returns random numbers from 0 - 36
     */
    public int getWinningNumber();
}
