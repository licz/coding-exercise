package org.test.gamesys.utils;

import org.test.gamesys.entities.Bet;
import org.test.gamesys.entities.Player;

import java.math.BigDecimal;

/**
 * Created by lich on 11/1/15.
 */
public class TestUtils {

    public static Player generatePlayer(String name) {
        Player player = new Player();
        player.setName(name);
        return player;
    }

    public static Bet generateBet(String owner, int number, String oddEven, BigDecimal amount) {
        Bet bet = new Bet();
        bet.setOwner(owner);
        bet.setNumber(number);
        bet.setOddEven(oddEven);
        bet.setAmount(amount);
        return bet;
    }
}
