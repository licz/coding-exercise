package org.test.gamesys.api.implementations;

import org.junit.Before;
import org.junit.Test;
import org.test.gamesys.api.DataParser;
import org.test.gamesys.entities.Player;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by lich on 10/1/15.
 */
public class DataParserImplementationTest {

    DataParser dataParser;
    List<Player> players;

    @Before
    public void setUp() {
        dataParser = new DataParserImplementation();

        players = new ArrayList<>();
        Player player = new Player();
        player.setName("gamesys");
        players.add(player);
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionOnEmptyPlayerName() {
        dataParser.parsePlayer("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionOnTooLongPlayerLine() {
        dataParser.parsePlayer("gamesys,1,2,3,4");
    }

    @Test(expected = NumberFormatException.class)
    public void shouldThrowExceptionOnIncorrectTotalWin() {
        dataParser.parsePlayer("gamesys,x,3");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionOnIncorrectName() {
        dataParser.parseBet("gamesys1 ODD 5", players);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionOnInccorectBet1() {
        dataParser.parseBet("gamesys ODDx 5", players);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionOnInccorectBet2() {
        dataParser.parseBet("gamesys -5 5", players);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionOnInccorectBet3() {
        dataParser.parseBet("gamesys 37 5", players);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionOnInccorectAmount1() {
        dataParser.parseBet("gamesys ODD -5", players);
    }

    @Test(expected = NumberFormatException.class)
    public void shouldThrowExceptionOnInccorectAmount2() {
        dataParser.parseBet("gamesys ODD x", players);
    }
}