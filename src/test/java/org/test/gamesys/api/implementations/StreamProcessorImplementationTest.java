package org.test.gamesys.api.implementations;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.test.gamesys.Main;
import org.test.gamesys.api.StreamProcessor;
import org.test.gamesys.entities.Player;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by lich on 11/1/15.
 */
public class StreamProcessorImplementationTest {
    private StreamProcessor streamProcessor;

    @Before
    public void setUp() {
        streamProcessor = new StreamProcessorImplementation(new DataParserImplementation());
    }

    @Test
    public void shouldReadStreamAndReturnPlayers() {
        List<Player> players;

        InputStream inputStream = this.getClass().getClassLoader()
                .getResourceAsStream(Main.PLAYERS_FILE);
        players = streamProcessor.readStream(inputStream);

        //should create 3 players
        Assert.assertEquals(3, players.size());

        //should assign Total values
        Assert.assertTrue(players.get(0).hasTotalValues());
        Assert.assertTrue(players.get(0).getTotalWin().compareTo(new BigDecimal(1)) == 0);
        Assert.assertTrue(players.get(0).getTotalBet().compareTo(new BigDecimal(2)) == 0);
    }
}