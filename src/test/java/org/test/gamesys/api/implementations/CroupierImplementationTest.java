package org.test.gamesys.api.implementations;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.test.gamesys.api.Croupier;
import org.test.gamesys.entities.Bet;
import org.test.gamesys.entities.Player;
import org.test.gamesys.entities.Win;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.test.gamesys.utils.TestUtils.generateBet;
import static org.test.gamesys.utils.TestUtils.generatePlayer;

/**
 * Created by lich on 10/1/15.
 */
public class CroupierImplementationTest {

    private Croupier croupier;
    Map<Player, List<Bet>> bets;
    Player player;

    @Before
    public void setUp() {
        croupier = new CroupierImplementation();
        bets = new HashMap<>();
        player = generatePlayer("gamesys");
        bets.put(player, new ArrayList<Bet>());
    }

    @Test
    public void shouldCalculateWinnerWithOdd() {
        bets.get(player).add(generateBet("gamesys", -1, Bet.ODD, new BigDecimal(5)));

        croupier.collectBets(bets);
        Map<Player, List<Win>> wins = croupier.calculateWinners(7);

        //should have one player
        Assert.assertEquals(1, wins.entrySet().size());

        //should have one win
        Assert.assertEquals(1, wins.get(player).size());
        Assert.assertEquals(Win.WIN, wins.get(player).get(0).getOutcome());

        //should win 10
        Assert.assertTrue(wins.get(player).get(0).getWonAmount().compareTo(new BigDecimal(10)) == 0);
    }

    @Test
    public void shouldCalculateWinnerWithTwoEvens() {
        bets.get(player).add(generateBet("gamesys", -1, Bet.EVEN, new BigDecimal(7)));
        bets.get(player).add(generateBet("gamesys", -1, Bet.EVEN, new BigDecimal(4)));

        croupier.collectBets(bets);
        Map<Player, List<Win>> wins = croupier.calculateWinners(10);

        //should have one player
        Assert.assertEquals(1, wins.entrySet().size());

        //should have two wins
        Assert.assertEquals(wins.get(player).size(), 2);
        Assert.assertEquals(Win.WIN, wins.get(player).get(0).getOutcome());
        Assert.assertEquals(Win.WIN, wins.get(player).get(1).getOutcome());

        //should win 14 and 8
        Assert.assertTrue(wins.get(player).get(0).getWonAmount().compareTo(new BigDecimal(14)) == 0 );
        Assert.assertTrue(wins.get(player).get(1).getWonAmount().compareTo(new BigDecimal(8)) == 0 );
    }

    @Test
    public void shouldCalculateTwoWinnersWithNumber() {

        Player testPlayer = generatePlayer("Leszek");
        bets.put(testPlayer, new ArrayList<Bet>());

        bets.get(player).add(generateBet("gamesys", 23, "", new BigDecimal(4)));
        bets.get(testPlayer).add(generateBet("Leszek", 23, "", new BigDecimal(7)));

        croupier.collectBets(bets);
        Map<Player, List<Win>> wins = croupier.calculateWinners(23);

        //should have two players
        Assert.assertEquals(2, wins.entrySet().size());

        //should have one win per player
        Assert.assertEquals(1, wins.get(player).size());
        Assert.assertEquals(Win.WIN, wins.get(player).get(0).getOutcome());
        Assert.assertEquals(1, wins.get(testPlayer).size());
        Assert.assertEquals(Win.WIN, wins.get(testPlayer).get(0).getOutcome());

        //should win 144 and 252
        Assert.assertTrue(wins.get(player).get(0).getWonAmount().compareTo(new BigDecimal(144)) == 0 );
        Assert.assertTrue(wins.get(testPlayer).get(0).getWonAmount().compareTo(new BigDecimal(252)) == 0 );
    }

    @Test
    public void shouldCalculateLooserWithEven() {
        bets.get(player).add(generateBet("gamesys", -1, Bet.EVEN, new BigDecimal(7)));
        bets.get(player).add(generateBet("gamesys", -1, Bet.EVEN, new BigDecimal(4)));

        croupier.collectBets(bets);
        Map<Player, List<Win>> wins = croupier.calculateWinners(11);

        //should have one player
        Assert.assertEquals(1, wins.entrySet().size());

        //should have two lose
        Assert.assertEquals(2, wins.get(player).size());
        Assert.assertEquals(Win.LOSE, wins.get(player).get(0).getOutcome());
        Assert.assertEquals(Win.LOSE, wins.get(player).get(1).getOutcome());
    }

    @Test
    public void shouldCalculateLooserWithEvenAndZero() {

        bets.get(player).add(generateBet("gamesys", -1, Bet.EVEN, new BigDecimal(4)));

        croupier.collectBets(bets);
        Map<Player, List<Win>> wins = croupier.calculateWinners(0);

        //should have one player
        Assert.assertEquals(1, wins.entrySet().size());

        //should have one lose
        Assert.assertEquals(1, wins.get(player).size());
        Assert.assertEquals(Win.LOSE, wins.get(player).get(0).getOutcome());
    }

    @Test
    public void shouldCalculateLooserWithEvenAndWinnerWithOdd() {

        bets.get(player).add(generateBet("gamesys", -1, Bet.EVEN, new BigDecimal(4)));
        bets.get(player).add(generateBet("gamesys", -1, Bet.ODD, new BigDecimal(7)));

        croupier.collectBets(bets);
        Map<Player, List<Win>> wins = croupier.calculateWinners(5);

        //should have one player
        Assert.assertEquals(1, wins.entrySet().size());

        //should have 2 games
        Assert.assertEquals(2, wins.get(player).size());

        //should have one lose
        Assert.assertEquals(Win.LOSE, wins.get(player).get(0).getOutcome());
        Assert.assertEquals(Bet.EVEN, wins.get(player).get(0).getBet());


        //should have one win
        Assert.assertEquals(Win.WIN, wins.get(player).get(1).getOutcome());
        Assert.assertEquals(Bet.ODD, wins.get(player).get(1).getBet());

    }
}
