package org.test.gamesys.api.implementations;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.test.gamesys.api.DataParser;
import org.test.gamesys.api.UserInterface;
import org.test.gamesys.entities.Bet;
import org.test.gamesys.entities.Player;
import static org.mockito.Mockito.*;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import static org.test.gamesys.utils.TestUtils.generateBet;
import static org.test.gamesys.utils.TestUtils.generatePlayer;

/**
 * Created by lich on 10/1/15.
 */
public class UserInterfaceImplementationTest {

    private UserInterface userInterface;

    @Test
    public void shouldAssignBet() {
        List<Player> players = new ArrayList<>();
        Player player = generatePlayer("gamesys");
        players.add(player);
        String mockedLine = "mock";

        ByteArrayInputStream in = new ByteArrayInputStream(mockedLine.getBytes());

        DataParser dataParser = mock(DataParser.class);
        Bet bet = generateBet("gamesys", -1, Bet.EVEN, BigDecimal.ONE);
        when(dataParser.parseBet(mockedLine, players)).thenReturn(bet);

        userInterface = new UserInterfaceImplementation(players, dataParser, new Scanner(in));

        Runnable runnable = (Runnable) userInterface;
        try {
            runnable.run();
        } catch (NoSuchElementException e) {

        }

        //should have one owner
        Assert.assertEquals(1, userInterface.getBets().entrySet().size());

        //should have one bet assigned to player
        Assert.assertEquals(1, userInterface.getBets().get(player).size());
    }

    @Test
    public void shouldUpdateTotalResults() {

    }
}